PostgreSQL is needed to run the application.
database configs are mentioned below.
spring.datasource.url=jdbc:postgresql://localhost:5432/postgres
spring.datasource.username=postgres
spring.datasource.password=postgres

The database can also be created by docker
sudo docker run --name postgresql-container -p 5432:5432 -e POSTGRES_PASSWORD=postgres -d postgres

data.sql file is being run so 2 types of roles(USER, ADMIN) will be created and also creating ADMIN user with login : admin and password : admin