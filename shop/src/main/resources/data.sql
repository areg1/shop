


INSERT INTO "role" (id, "name")
	VALUES (1, 'ADMIN');
INSERT INTO "role" (id, "name")
	VALUES (2, 'USER');

INSERT INTO users (id, is_blocked, password, user_name, role_id)
	VALUES (1, false, '$2a$10$/Y6IlI7mTVlIeXdvG.ZfA.WA7xJri6eiVQeZISxUBwA7jLEFunMBa', 'admin', 1);