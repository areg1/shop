package com.areg.shop.resource;

import com.areg.shop.dto.BlockOrUnblockUserRequestDTO;
import com.areg.shop.dto.CreateProductDTO;
import com.areg.shop.model.Product;
import com.areg.shop.model.User;
import com.areg.shop.service.ProductService;
import com.areg.shop.service.UserService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@RequestMapping("/api/v1/admin")
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @PutMapping(path = "/blockOrUnblock", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> blockOrUnblockUser(
            final @RequestBody BlockOrUnblockUserRequestDTO blockOrUnblockUserRequestDTO
    ) {

        Optional<User> user = this.userService.blockOrUnblockUser(blockOrUnblockUserRequestDTO);

        if (!user.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(user.get());
    }

    @PostMapping(path = "/createProduct", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createProduct(
            final @RequestBody CreateProductDTO createProductDTO
    ) {

        Optional<Product> product = this.productService.createProduct(createProductDTO);

        if (!product.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(product);
    }

}
