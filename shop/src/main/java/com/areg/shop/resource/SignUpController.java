package com.areg.shop.resource;

import com.areg.shop.dto.CreateProductDTO;
import com.areg.shop.dto.CreateUserDTO;
import com.areg.shop.model.Product;
import com.areg.shop.model.User;
import com.areg.shop.service.UserService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author areg
 */
@Controller
@RestController
public class SignUpController {
    
    @Autowired
    private UserService userService;
    
    
    @PostMapping(path = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(
            final @RequestBody CreateUserDTO createUserDTO
    ) {

        Optional<User> user = this.userService.createUser(createUserDTO);

        if (!user.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(user);
    }
    
}
