package com.areg.shop.resource;

import com.areg.shop.dto.CreateProductReviewDTO;
import com.areg.shop.model.Product;
import com.areg.shop.model.ProductReview;
import com.areg.shop.service.ProductReviewService;
import com.areg.shop.service.ProductService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author areg
 */
@Controller
@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    
    @Autowired
    private ProductService productService;
    
    @Autowired
    private ProductReviewService productReviewService;
    
    @GetMapping(path = "/product/list", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllProducts() {

        List<Product> products = this.productService.getAllProducts();

        return ResponseEntity.status(HttpStatus.OK).body(products);
    }
    
    @PostMapping(path = "/createProductReview", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createProduct(
            final @RequestBody CreateProductReviewDTO createProductReviewDTO
    ) {

        Optional<ProductReview> productReview = this.productReviewService.createProductReview(createProductReviewDTO);

        if (!productReview.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.status(HttpStatus.OK).body(productReview);
    }
    
    
}
