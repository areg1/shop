package com.areg.shop.model;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="Users")
public class User implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "roleId")
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String userName;
    
    private String password;
    
    private boolean isBlocked;

    public User() {
    }

    public User(Integer id, Role role, String userName, String password, boolean isBlocked) {
        this.id = id;
        this.role = role;
        this.userName = userName;
        this.password = password;
        this.isBlocked = isBlocked;
    }
    
    
    
    public User(Integer id, String username, String password, boolean isBlocked) {
        this.id = id;
        this.userName = username;
        this.password = password;
        this.isBlocked = isBlocked;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return userName;
    }

    public void setUsername(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", role=" + role + ", userName=" + userName + ", password=" + password + ", isBlocked=" + isBlocked + '}';
    }

}
