package com.areg.shop.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author areg
 */
@Entity
@Table(name="Product")
public class Product implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Category category;
    
    private String name;
    
    private Double price;
    
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private Set<ProductReview> productReviews;

    public Product() {
    }

    public Product(Integer id, Category category, String name, Double price, Set<ProductReview> productReviews) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.price = price;
        this.productReviews = productReviews;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", category=" + category + ", name=" + name + ", price=" + price + ", productReviews=" + productReviews + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Set<ProductReview> getProductReviews() {
        return productReviews;
    }

    public void setProductReviews(Set<ProductReview> productReviews) {
        this.productReviews = productReviews;
    }
    
    
    
    
    
    
}
