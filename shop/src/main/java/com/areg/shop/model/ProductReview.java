package com.areg.shop.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author areg
 */
@Entity
@Table(name="ProductReview")
public class ProductReview implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;
    
    private String comment;
    
    private Double rate;

    public ProductReview() {
    }

    public ProductReview(Integer id, Product product, String comment, Double rate) {
        this.id = id;
        this.product = product;
        this.comment = comment;
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "ProductReview{" + "id=" + id + ", comment=" + comment + ", rate=" + rate + '}';
    }
    
    
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
    
    
    
}
