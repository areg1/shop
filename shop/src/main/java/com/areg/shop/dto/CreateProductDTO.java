package com.areg.shop.dto;

/**
 *
 * @author areg
 */
public class CreateProductDTO {
    
    private Integer categoryId;
    
    private String categoryName;
    
    private String name;
    
    private Double price;

    public CreateProductDTO() {
    }

    public CreateProductDTO(Integer categoryId, String categoryName, String name, Double price) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.name = name;
        this.price = price;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
    
    
    
}
