package com.areg.shop.dto;

/**
 *
 * @author areg
 */
public class CreateProductReviewDTO {
    
    private Integer productId;
    
    private Double rate;
    
    private String comment;

    public CreateProductReviewDTO(Integer productId, Double rate, String comment) {
        this.productId = productId;
        this.rate = rate;
        this.comment = comment;
    }

    public CreateProductReviewDTO() {
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
    
    
    
}
