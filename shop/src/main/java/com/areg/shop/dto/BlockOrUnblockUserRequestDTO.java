package com.areg.shop.dto;

/**
 *
 * @author areg
 */
public class BlockOrUnblockUserRequestDTO {
    
    private Integer userId;
    
    private Boolean isBlocked;

    public BlockOrUnblockUserRequestDTO(Integer userId, Boolean isBlocked) {
        this.userId = userId;
        this.isBlocked = isBlocked;
    }

    public BlockOrUnblockUserRequestDTO() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }
    
}
