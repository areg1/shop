package com.areg.shop.dto;

/**
 *
 * @author areg
 */
public class CreateUserDTO {
    
    
    private String userName;
    
    private String password;

    public CreateUserDTO() {
    }

    public CreateUserDTO(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
    
    

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
