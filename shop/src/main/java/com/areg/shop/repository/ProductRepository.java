package com.areg.shop.repository;

import com.areg.shop.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author areg
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    
}
