package com.areg.shop.repository;

import com.areg.shop.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author areg
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    
}
