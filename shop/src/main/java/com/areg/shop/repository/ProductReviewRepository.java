package com.areg.shop.repository;

import com.areg.shop.model.ProductReview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author areg
 */
@Repository
public interface ProductReviewRepository extends JpaRepository<ProductReview, Integer> {
    
}
