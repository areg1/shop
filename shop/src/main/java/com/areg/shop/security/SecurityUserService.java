package com.areg.shop.security;


import com.areg.shop.model.Role;
import com.areg.shop.model.User;
import com.areg.shop.repository.RoleRepository;
import com.areg.shop.repository.UserRepository;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class SecurityUserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(userName);
        
        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));
        
        Role role = user.get().getRole();
        
        if(role == null){
            throw new UsernameNotFoundException("Not found role.");
        }
        
        return user.map(u -> new SecurityUser(u, Arrays.asList(role.getName()))).get();
    }
}