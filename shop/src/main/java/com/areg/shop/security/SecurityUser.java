package com.areg.shop.security;

import com.areg.shop.model.User;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author areg
 */
public class SecurityUser implements UserDetails{
    
    private String userName;
    
    private String password;
    
    private boolean isBlocked;
    
    private List<GrantedAuthority> authorities;

    public SecurityUser(User user, List<String> roles) {
        this.userName = user.getUsername();
        this.password = user.getPassword();
        this.isBlocked = user.getIsBlocked();
        this.authorities = roles.stream()
                    .map((String s) -> new SimpleGrantedAuthority(s))
                    .collect(Collectors.toList());
        
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !isBlocked;
    }
    
}
