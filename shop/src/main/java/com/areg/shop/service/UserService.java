package com.areg.shop.service;

import com.areg.shop.dto.BlockOrUnblockUserRequestDTO;
import com.areg.shop.dto.CreateUserDTO;
import com.areg.shop.model.User;
import com.areg.shop.repository.RoleRepository;
import com.areg.shop.repository.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author areg
 */
@Service
public class UserService {
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Optional<User> blockOrUnblockUser(BlockOrUnblockUserRequestDTO blockOrUnblockUserRequestDTO) {
        // validate request body        
        if(blockOrUnblockUserRequestDTO == null || blockOrUnblockUserRequestDTO.getUserId() == null || blockOrUnblockUserRequestDTO.getBlocked() == null){
            return Optional.empty();
        }
        
        Optional<User> user = userRepository.findById(blockOrUnblockUserRequestDTO.getUserId());
        
        // if user not found return 
        if(!user.isPresent()){
            return Optional.empty();
        }
        
        user.get().setIsBlocked(blockOrUnblockUserRequestDTO.getBlocked());
        
        return Optional.of(this.userRepository.save(user.get()));
    }

    public Optional<User> createUser(CreateUserDTO createUserDTO) {
        // Validation        
        if(createUserDTO == null || createUserDTO.getPassword() == null || createUserDTO.getUserName() == null){
            return Optional.empty();
        }
        
        Optional<User> findByUserName = this.userRepository.findByUserName(createUserDTO.getUserName());
        
        if(findByUserName.isPresent()){
            // there is already registered user with given name           
            return Optional.empty();
        }
        
        // Create user with "USER" role        
        User user = new User(null, this.roleRepository.findByName("USER").get(), createUserDTO.getUserName(), this.passwordEncoder.encode(createUserDTO.getPassword()), false);
        
        return Optional.of(this.userRepository.save(user));
        
    }
    
}
