package com.areg.shop.service;

import com.areg.shop.dto.CreateProductReviewDTO;
import com.areg.shop.model.Product;
import com.areg.shop.model.ProductReview;
import com.areg.shop.repository.ProductRepository;
import com.areg.shop.repository.ProductReviewRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author areg
 */
@Service
public class ProductReviewService {
    
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ProductReviewRepository productReviewRepository;

    public Optional<ProductReview> createProductReview(CreateProductReviewDTO createProductReviewDTO) {
        
        if(createProductReviewDTO == null || createProductReviewDTO.getProductId() == null){
            return Optional.empty();
        }
        
        Optional<Product> product = productRepository.findById(createProductReviewDTO.getProductId());
        
        if(!product.isPresent()){
            return Optional.empty();
        }
        
        ProductReview productReview = new ProductReview(null, product.get(), createProductReviewDTO.getComment(), createProductReviewDTO.getRate());
        
        return Optional.of(this.productReviewRepository.save(productReview));
    }
    
}
