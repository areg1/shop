package com.areg.shop.service;

import com.areg.shop.dto.CreateProductDTO;
import com.areg.shop.model.Category;
import com.areg.shop.model.Product;
import com.areg.shop.repository.CategoryRepository;
import com.areg.shop.repository.ProductRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author areg
 */
@Service
public class ProductService {
    
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;
   

    public Optional<Product> createProduct(CreateProductDTO createProductDTO) {
        // validation
        if (createProductDTO == null || createProductDTO.getName() == null || createProductDTO.getPrice() == null) {
            return Optional.empty();
        }

        if (createProductDTO.getCategoryId() != null) {
            // getting existing category
            Optional<Category> category = this.categoryRepository.findById(createProductDTO.getCategoryId());

            if (!category.isPresent()) {
                return Optional.empty();
            }

            Product product = new Product(null, category.get(), createProductDTO.getName(), createProductDTO.getPrice(), null);
            // cretaing product            
            return Optional.of(this.productRepository.save(product));

        } else {

            Category category = new Category(null, createProductDTO.getCategoryName(), null);

            // cretaing new category
            category = this.categoryRepository.save(category);

            Product product = new Product(null, category, createProductDTO.getName(), createProductDTO.getPrice(), null);

            // cretaing product
            return Optional.of(this.productRepository.save(product));

        }

    }

    public List<Product> getAllProducts() {        
        return this.productRepository.findAll();
    }

}
